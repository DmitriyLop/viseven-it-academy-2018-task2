let space = document.getElementById("space");
let sea = document.getElementById("sea");
let ship = document.getElementsByClassName("ship")[0];

if(ship){
    ship.addEventListener("click",transformToSpaceship);
}

 function transformToSpaceship (event) {
    let ship = event.currentTarget;
    ship.classList.remove("submarine");
    ship.classList.add("spaceship");
    space.classList.add("section-moving");
    sea.classList.add("section-moving");
}
